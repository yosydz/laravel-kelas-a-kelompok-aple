<?php

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\KegiatanController;
use App\Http\Controllers\PeminjamController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PenjagaController;
use App\Http\Controllers\TimController;

// use App\Http\Controllers\KegiatanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);

    //TIM
    Route::resource('tim', TimController::class);

    //PENJAGA
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/penjaga/data', [App\Http\Controllers\PenjagaController::class, 'data'])->name('data');
    Route::resource('penjaga', PenjagaController::class);

    // KEGIATAN
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    // PEMINJAM
    Route::get('/peminjam', [App\Http\Controllers\PeminjamController::class, 'index'])->name('peminjam');
    Route::get('/peminjam/create', [PeminjamController::class, 'create'])->name('peminjam_tambah');
    Route::get('/peminjam/edit/{id_peminjam}', [PeminjamController::class, 'edit'])->name('peminjam_edit');

    Route::post('/peminjam/store', [PeminjamController::class, 'store'])->name('peminjam_store');
    Route::put('/peminjam/update/{id_peminjam}', [App\Http\Controllers\PeminjamController::class, 'update'])->name('peminjam_update');
    Route::get('/peminjam/destroy/{id_peminjam}', [App\Http\Controllers\PeminjamController::class, 'destroy'])->name('peminjam_delete');

    //gedung
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/gedung', [App\Http\Controllers\GedungController::class, 'index'])->name('gedung');
    Route::get('/gedung/tambah', [App\Http\Controllers\GedungController::class, 'tambah'])->name('gedung_tambah');
    Route::get('/gedung/edit/{id_gedung}', [App\Http\Controllers\GedungController::class, 'edit'])->name('gedung_edit');
    // Operation
    Route::post('/gedung/store', [App\Http\Controllers\GedungController::class, 'store'])->name('gedung_store');
    Route::put('/gedung/update/{id_gedung}', [App\Http\Controllers\GedungController::class, 'update'])->name('gedung_update');
    Route::get('/gedung/hapus/{id_gedung}', [App\Http\Controllers\GedungController::class, 'delete'])->name('gedung_delete');

    // kebersihan
    Route::get('/anggota_kebersihan', [App\Http\Controllers\AnggotaKebersihanController::class, 'index'])->name('anggota_kebersihan');
    Route::get('/anggota_kebersihan/create', [App\Http\Controllers\AnggotaKebersihanController::class, 'create'])->name('anggota_kebersihan_create');
    Route::get('/anggota_kebersihan/edit/{id}', [App\Http\Controllers\AnggotaKebersihanController::class, 'edit'])->name('anggota_kebersihan_edit');
    // Operation
    Route::post('/anggota_kebersihan/store', [App\Http\Controllers\AnggotaKebersihanController::class, 'store'])->name('anggota_kebersihan_store');
    Route::put('/anggota_kebersihan/update/{id}', [App\Http\Controllers\AnggotaKebersihanController::class, 'update'])->name('anggota_kebersihan_update');
    Route::get('/anggota_kebersihan/destroy/{id}', [App\Http\Controllers\AnggotaKebersihanController::class, 'destroy'])->name('anggota_kebersihan_delete');


    //rekap
    Route::get('/rekap', 'App\Http\Controllers\RekapController@index')->name('rekap');
    Route::get('/rekap/tambah', 'App\Http\Controllers\RekapController@create')->name('tambah');
    Route::get('/rekap/edit/{id_rekap}', 'App\Http\Controllers\RekapController@edit')->name('edit');
    Route::put('/rekap/update/{id_rekap}', 'App\Http\Controllers\RekapController@update')->name('update');
    Route::get('/rekap/delete/{id_rekap}', 'App\Http\Controllers\RekapController@destroy')->name('delete');
    Route::post('/rekap', 'App\Http\Controllers\RekapController@store')->name('rekap');

    // KEGIATAN
    // Route::get('/kegiatan', [App\Http\Controllers\KegiatanController::class, 'index'])->name('kegiatan');
    Route::get('/kegiatan', [App\Http\Controllers\KegiatanController::class, 'index'])->name('kegiatan');
    Route::get('/kegiatan/tambah', [App\Http\Controllers\KegiatanController::class, 'tambah'])->name('kegiatan_tambah');
    Route::get('/kegiatan/edit/{id_kegiatan}', [App\Http\Controllers\KegiatanController::class, 'edit'])->name('kegiatan_edit');
    // Operation
    Route::post('/kegiatan/store', [App\Http\Controllers\KegiatanController::class, 'store'])->name('kegiatan_store');
    Route::put('/kegiatan/update/{id_kegiatan}', [App\Http\Controllers\KegiatanController::class, 'update'])->name('kegiatan_update');
    Route::get('/kegiatan/hapus/{id_kegiatan}', [App\Http\Controllers\KegiatanController::class, 'delete'])->name('kegiatan_delete');
});
