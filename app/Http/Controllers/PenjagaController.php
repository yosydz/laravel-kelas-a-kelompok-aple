<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use App\Models\Penjaga;
use Illuminate\Http\Request;

class PenjagaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('penjaga.index')->with('headerdata', $headerdata);

    }

    public function data(Penjaga $penjaga)
    {
        $data = $penjaga->getData();
        return \DataTables::of($data)
            ->addColumn('Actions', function($data) {
                return '<button type="button" onclick="show('.$data->id_penjaga.')" class="btn btn-primary btn-sm" id="getDetailId"><i class="fa fa-eye"></i></button>
                <button type="button" class="btn btn-success btn-sm" onclick="edit('.$data->id_penjaga.')" data-id="'.$data->id_penjaga.'" id="getEditPenjagaData"><i class="fa fa-edit"></i></button>
                <button type="button" data-id="'.$data->id_penjaga.'" data-toggle="modal" data-target="#DeletePenjagaModal" class="btn btn-danger btn-sm" id="getDeleteId"><i class="fa fa-trash"></i></button>';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Penjaga $penjaga)
    {
        $validator = \Validator::make($request->all(), [
            'nama_penjaga' => 'required',
            'ktp' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'nomor_hp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $penjaga->storeData($request->all());

        return response()->json(['success'=>'Penjaga added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Penjaga  $penjaga
     * @return \Illuminate\Http\Response
     */
    public function show(Penjaga $penjaga)
    {
        if(!empty($penjaga->id_penjaga)){

            $data['error'] = 0;
            $data['id_penjaga'] = $penjaga->id_penjaga;
            $data['nama_penjaga'] = $penjaga->nama_penjaga;
            $data['ktp'] = $penjaga-> ktp;
            $data['tempat_lahir'] = $penjaga->tempat_lahir;
            $data['tanggal_lahir'] = $penjaga->tanggal_lahir;
            $data['jenis_kelamin'] = $penjaga->jenis_kelamin;
            $data['alamat'] = $penjaga->alamat;
            $data['nomor_hp'] = $penjaga->nomor_hp;
            // dd($data);
            return response()->json($data);
        }

        return response()->json($data['errors'] = 1);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Penjaga  $penjaga
     * @return \Illuminate\Http\Response
     */
    public function edit(Penjaga $penjaga)
    {
        $penjaga = new Penjaga;
        $data = $penjaga->findData($id);

        $html = '<div class="form-group">
                    <label for="Title">Title:</label>
                    <input type="text" class="form-control" name="title" id="editTitle" value="'.$data->title.'">
                </div>
                <div class="form-group">
                    <label for="Name">Description:</label>
                    <textarea class="form-control" name="description" id="editDescription">'.$data->description.'
                    </textarea>
                </div>';

        return response()->json(['html'=>$html]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Penjaga  $penjaga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penjaga $penjaga)
    {
        $validator = \Validator::make($request->all(), [
            'nama_penjaga' => 'required',
            'ktp' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'nomor_hp' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }

        $id = $penjaga->id_penjaga;

        $penjaga = new Penjaga;
        $penjaga->updateData($id, $request->all());

        return response()->json(['success'=>'Penjaga updated successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Penjaga  $penjaga
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penjaga $penjaga)
    {
        $id = $penjaga->id_penjaga;
        $penjaga = new Penjaga;
        $penjaga->deleteData($id);

        return response()->json(['success'=>'Penjaga deleted successfully']);
    }
}
