<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use App\Models\Peminjam;
use Illuminate\Http\Request;

class PeminjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peminjam = Peminjam::get();
        // $kegiatan = Kegiatan::paginate(3);
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('peminjam.index')->with('peminjam', $peminjam)->with('headerdata', $headerdata);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('peminjam.peminjam_tambah')->with('headerdata', $headerdata);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_peminjam' => 'required',
            'status' => 'required',
            'nim_ktp' => 'required',
            'jenis_kelamin' => 'required',
            'nomor_hp' => 'required',
            'email' => 'required',
        ]);

        Peminjam::create([
            'nama_peminjam' => $request->nama_peminjam,
            'status' => $request->status,
            'nim_ktp' => $request->nim_ktp,
            'jenis_kelamin' => $request->jenis_kelamin,
            'nomor_hp' => $request->nomor_hp,
            'email' => $request->email,
        ]);

        return redirect('/peminjam');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_peminjam)
    {
        $peminjam = Peminjam::find($id_peminjam);
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('peminjam.peminjam_edit')->with('peminjam', $peminjam)->with('headerdata', $headerdata);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_peminjam)
    {
        $this->validate($request, [
            'nama_peminjam' => 'required',
            'status' => 'required',
            'nim_ktp' => 'required',
            'jenis_kelamin' => 'required',
            'nomor_hp' => 'required',
            'email' => 'required',
        ]);

        $peminjam = Peminjam::find($id_peminjam);
        $peminjam->nama_peminjam = $request->nama_peminjam;
        $peminjam->status = $request->status;
        $peminjam->nim_ktp = $request->nim_ktp;
        $peminjam->jenis_kelamin = $request->jenis_kelamin;
        $peminjam->nomor_hp = $request->nomor_hp;
        $peminjam->email = $request->email;
        $peminjam->save();

        return redirect('/peminjam');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_peminjam)
    {
        $peminjam = Peminjam::find($id_peminjam);
        $peminjam->delete();
        return redirect('/peminjam');

    }
}
