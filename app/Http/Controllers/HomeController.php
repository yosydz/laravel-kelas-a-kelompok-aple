<?php

namespace App\Http\Controllers;

use App\Models\Gedung;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('dashboard')->with('headerdata', $headerdata);
    }
}
