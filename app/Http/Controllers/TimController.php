<?php

namespace App\Http\Controllers;

use App\Models\Tim;
use App\Models\Gedung;
use Illuminate\Http\Request;

class TimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = tim::orderBy('nama_tim', "ASC")->get();
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('tim.index',compact('datas'))->with('headerdata', $headerdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view("tim.create")->with('headerdata', $headerdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_tim' => 'required',
        ]);

        Tim::create($request->all());

        return redirect()->route('tim.index')->with('success', 'Tim created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tim  $tim
     * @return \Illuminate\Http\Response
     */
    public function show(Tim $tim)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tim  $tim
     * @return \Illuminate\Http\Response
     */
    public function edit(Tim $tim)
    {
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('tim.edit', compact('tim'))->with('headerdata', $headerdata);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tim  $tim
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tim $tim)
    {
        $request->validate([
            'nama_tim' => 'required',
        ]);

        $tim->update($request->all());
        return redirect()->route('tim.index')->with('success', 'Tim has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tim  $tim
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tim $tim)
    {
        $delete = $tim->delete();
        return redirect()->route('tim.index')->with('success','Tim ha been deleted successfully');
    }
}
