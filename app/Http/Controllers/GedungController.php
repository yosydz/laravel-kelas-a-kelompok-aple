<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use Illuminate\Http\Request;

class GedungController extends Controller
{
    public function index()
    {
        // $penjaga = Gedung::with(['penjaga'])->first();
        // dd($penjaga->nama_penajaga);
        $gedung = Gedung::all();
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        // $gedung = Gedung::paginate(3);
        return view('gedung.index')->with('gedung', $gedung)->with('headerdata', $headerdata);
    }

    public function tambah()
    {
        $penjaga = \App\Models\Penjaga::get();
        $tim = \App\Models\Tim::get();
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('gedung.gedung_tambah')->with('penjaga', $penjaga)->with('tim', $tim)->with('headerdata', $headerdata);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_gedung' => 'required',
            'kapasitas' => 'required',
            'fasilitas' => 'required',
            'luas' => 'required',
            'm_id_penjaga' => 'required',
            'm_id_tim_kebersihan' => 'required',
        ]);

        $add = Gedung::create([
            'nama_gedung' => $request->nama_gedung,
            'kapasitas' => $request->kapasitas,
            'fasilitas' => $request->fasilitas,
            'luas' => $request->luas,
            'm_id_penjaga' => $request->m_id_penjaga,
            'm_id_tim_kebersihan' => $request->m_id_tim_kebersihan,
        ]);

        // dd($add);

        return redirect('/gedung');
    }

    public function edit($id_gedung)
    {
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        $gedung = Gedung::find($id_gedung);
        $penjaga = \App\Models\Penjaga::get();
        $tim = \App\Models\Tim::get();
        return view('gedung.gedung_edit')->with('gedung', $gedung)->with('penjaga', $penjaga)->with('tim', $tim)->with('headerdata', $headerdata);
    }

    public function update($id_gedung, Request $request)
    {
        $this->validate($request, [
            'nama_gedung' => 'required',
            'kapasitas' => 'required',
            'fasilitas' => 'required',
            'luas' => 'required',
            'm_id_penjaga' => 'required',
            'm_id_tim_kebersihan' => 'required',
        ]);

        $gedung = Gedung::find($id_gedung);
        $gedung->nama_gedung = $request->nama_gedung;
        $gedung->kapasitas = $request->kapasitas;
        $gedung->fasilitas = $request->fasilitas;
        $gedung->luas = $request->luas;
        $gedung->m_id_penjaga = $request->m_id_penjaga;
        $gedung->m_id_tim_kebersihan = $request->m_id_tim_kebersihan;
        $gedung->save();
        return redirect('/gedung');

    }

    public function delete($id_gedung)
    {
        $gedung = Gedung::find($id_gedung);
        $gedung->delete();
        return redirect('/gedung');

    }
}
