<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use App\Models\Kegiatan;
use App\Models\Peminjam;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
    public function index()
    {
        // $peminjam = Peminjam::get();
        // $kegiatan = Kegiatan::get()->join('peminjam', 'kegiatan.m_id_peminjam', '=', 'peminjam.id_peminjam')
        // ->select('kegiatan.nama_kegiatan', 'peminjam.nama_peminjam');
        // var_dump($kegiatan); die();
        $kegiatan = Kegiatan::get();
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('kegiatan.index')->with('kegiatan', $kegiatan)->with('headerdata', $headerdata);
    }

    public function tambah()
    {
        $peminjam = Peminjam::orderBy('nama_peminjam', 'ASC')->get();
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('kegiatan.kegiatan_tambah')->with('peminjam', $peminjam)->with('headerdata', $headerdata);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_kegiatan' => 'required',
            'instansi' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_akhir' => 'required',
            'm_id_peminjam' => 'required',
        ]);

        Kegiatan::create([
            'nama_kegiatan' => $request->nama_kegiatan,
            'instansi' => $request->instansi,
            'tanggal_mulai' => $request->tanggal_mulai,
            'tanggal_akhir' => $request->tanggal_akhir,
            'm_id_peminjam' => $request->m_id_peminjam,
        ]);

        return redirect('/kegiatan')->with('status', 'Data Kegiatan Berhasil Ditambahkan');
    }

    public function edit($id_kegiatan)
    {
        $kegiatan = Kegiatan::find($id_kegiatan);
        $peminjam = Peminjam::orderBy('nama_peminjam', 'ASC')->get();
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('kegiatan.kegiatan_edit')->with('kegiatan', $kegiatan)->with('peminjam', $peminjam)->with('headerdata', $headerdata);
    }

    public function update($id_kegiatan, Request $request)
    {
        $this->validate($request, [
            'nama_kegiatan' => 'required',
            'instansi' => 'required',
            'tanggal_mulai' => 'required',
            'tanggal_akhir' => 'required',
            'm_id_peminjam' => 'required',
        ]);

        $kegiatan = Kegiatan::find($id_kegiatan);
        $kegiatan->nama_kegiatan = $request->nama_kegiatan;
        $kegiatan->instansi = $request->instansi;
        $kegiatan->tanggal_mulai = $request->tanggal_mulai;
        $kegiatan->tanggal_akhir = $request->tanggal_akhir;
        $kegiatan->m_id_peminjam = $request->m_id_peminjam;
        $kegiatan->save();
        return redirect('/kegiatan')->with('status', 'Data Kegiatan Berhasil Diedit');

    }

    public function delete($id_kegiatan)
    {
        $kegiatan = Kegiatan::find($id_kegiatan);
        $kegiatan->delete();
        return redirect('/kegiatan');

    }
}
