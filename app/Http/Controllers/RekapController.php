<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use App\Models\Rekap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $join = DB::table('rekap')
            ->join('kegiatan', 'rekap.m_id_kegiatan', '=', 'kegiatan.id_kegiatan')
            ->join('peminjam', 'kegiatan.m_id_peminjam', '=', 'peminjam.id_peminjam')
            ->join('gedung', 'rekap.m_id_gedung', '=', 'gedung.id_gedung')
            ->join('tim_kebersihan', 'gedung.m_id_tim_kebersihan', '=', 'tim_kebersihan.id')
            ->join('penjaga', 'gedung.m_id_penjaga', '=', 'penjaga.id_penjaga')
            ->select('rekap.*', 'peminjam.nama_peminjam', 'gedung.nama_gedung', 'tim_kebersihan.nama_tim', 'penjaga.nama_penjaga', 'peminjam.nama_peminjam', 'kegiatan.*')
            ->get();

        // dd($join);
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();

        return view('rekap.index', ['rekap' => $join])->with('headerdata', $headerdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $gedung = DB::table('gedung')->get();
        // dump($gedung);

        $kegiatan = DB::table('kegiatan')->get();
        // dump($kegiatan);
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();

        return view('rekap.tambah', ['gedung' => $gedung, 'kegiatan' => $kegiatan])->with('headerdata', $headerdata);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi data
        $request->validate([
            'm_id_gedung' => 'required',
            'm_id_kegiatan' => 'required',
        ]);

        //store in database
        Rekap::create($request->all());

        return redirect('/rekap')->with('status', 'Rekap Added Succesfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id_rekap)
    {
        //

        $gedung = DB::table('gedung')->get();
        // dump($gedung);

        $kegiatan = DB::table('kegiatan')->get();
        // dump($kegiatan);

        // $request->validate([
        //     'm_id_gedung'=>'required',
        //     'm_id_kegiatan'=>'required',
        // ]);
        // var_dump('$request');
        // die;

        $rekap = Rekap::find($id_rekap);
        // dd($rekap);
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('rekap.edit', ['gedung' => $gedung, 'kegiatan' => $kegiatan, 'rekap' => $rekap])->with('headerdata', $headerdata);

        // return view('posts.edit',compact('post'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_rekap)
    {
        //
        //validasi data
        $request->validate([
            'm_id_gedung' => 'required',
            'm_id_kegiatan' => 'required',
        ]);
        var_dump('$request');
        // die;

        // //store in database
        // Rekap::update($request->all());

        $rekap = Rekap::find($id_rekap);
        $rekap->m_id_gedung = $request->m_id_gedung;
        $rekap->m_id_kegiatan = $request->m_id_kegiatan;
        $rekap->save();

        return redirect('/rekap')->with('status', 'Rekap Added Succesfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_rekap)
    {
        //
        $rekap = Rekap::find($id_rekap);
        $rekap->delete();
        return redirect('/rekap')->with('status', 'Delete Succesfully!');

    }
}
