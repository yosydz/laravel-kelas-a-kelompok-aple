<?php

namespace App\Http\Controllers;

use App\Models\Tim;
use App\Models\Gedung;
use Illuminate\Http\Request;
use App\Models\AnggotaKebersihan;

class AnggotaKebersihanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $anggotakebersihan = AnggotaKebersihan::get();
        // $kebersihan = Kegiatan::paginate(3);
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('kebersihan.index')->with('anggotakebersihan', $anggotakebersihan)->with('headerdata', $headerdata);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tim = Tim::get();
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('kebersihan.kebersihan_tambah')->with('tim', $tim)->with('headerdata', $headerdata);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'nama' => 'required',
            'm_id_tim_kebersihan' => 'required',
            'ktp' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'nomor_hp' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }



        AnggotaKebersihan::create([
            'nama' => $request->nama,
            'm_id_tim_kebersihan' => $request->m_id_tim_kebersihan,
            'ktp' => $request->ktp,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'alamat' => $request->alamat,
            'nomor_hp' => $request->nomor_hp,

        ]);

        return redirect('anggota_kebersihan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggotakebersihan = AnggotaKebersihan::find($id);
        $tim = Tim::orderBy('nama_tim', 'ASC')->get();
        $headerdata = Gedung::orderBy('kapasitas', "DESC")->get();
        return view('kebersihan.kebersihan_edit')->with('anggotakebersihan', $anggotakebersihan)->with('tim', $tim)->with('headerdata', $headerdata);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'm_id_tim_kebersihan' => 'required',
            'ktp' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'alamat' => 'required',
            'nomor_hp' => 'required',
        ]);

        $anggotakebersihan = AnggotaKebersihan::find($id);
        $anggotakebersihan->nama = $request->nama;
        $anggotakebersihan->m_id_tim_kebersihan = $request->m_id_tim_kebersihan;
        $anggotakebersihan->ktp = $request->ktp;
        $anggotakebersihan->tempat_lahir = $request->tempat_lahir;
        $anggotakebersihan->tanggal_lahir = $request->tanggal_lahir;
        $anggotakebersihan->jenis_kelamin = $request->jenis_kelamin;
        $anggotakebersihan->alamat = $request->alamat;
        $anggotakebersihan->nomor_hp = $request->nomor_hp;
        $anggotakebersihan->save();
        return redirect('/anggota_kebersihan');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anggotakebersihan = AnggotaKebersihan::find($id);
        $anggotakebersihan->delete();
        return redirect('anggota_kebersihan');
    }
}
