<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kebersihan extends Model
{
    use HasFactory;

    protected $table = "anggota_kebersihan";
    protected $primaryKey = 'id_kebersihan';

    protected $fillable = ["nama_kebersihan", "ktp", "tempat_lahir", "tanggal_lahir", "jenis_kelamin", "alamat", "nomor_hp"];

    public function tim()
    {
        return $this->belongsTo(Tim::class, 'm_id_tim_kebersihan');
    }


}
