<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Penjaga extends Eloquent
{
    use HasFactory;

    protected $table = "penjaga";
    protected $primaryKey = 'id_penjaga';

    protected $fillable = ["nama_penjaga", "ktp", "tempat_lahir", "tanggal_lahir", "jenis_kelamin", "alamat", "nomor_hp"];

    public function getData()
    {
        return static::orderBy('created_at','desc')->get();
    }

    public function storeData($input)
    {
    	return static::create($input);
    }

    public function findData($id)
    {
        return static::find($id);
    }

    public function updateData($id, $input)
    {
        return static::find($id)->update($input);
    }

    public function deleteData($id)
    {
        return static::find($id)->delete();
    }
}
