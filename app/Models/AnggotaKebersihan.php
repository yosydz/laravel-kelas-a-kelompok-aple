<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnggotaKebersihan extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'anggota_kebersihan';

    // /**
    //  * The primary key for the model.
    //  *
    //  * @var string
    //  */
    // protected $primaryKey = 'id';

    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
        'm_id_tim_kebersihan',
        'ktp',
        'tempat_lahir',
        'tanggal_lahir',
        'jenis_kelamin',
        'alamat',
        'nomor_hp',
    ];

    public function tim()
    {
        return $this->belongsTo('App\Models\Tim', 'm_id_tim_kebersihan', 'id');
    }

}
