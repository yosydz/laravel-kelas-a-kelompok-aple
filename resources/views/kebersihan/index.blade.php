@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    {{-- <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style> --}}

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Anggota Kebersihan</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="/anggota_kebersihan/create" class="btn btn-sm btn-primary">Tambah kebersihan</a>
                            </div>
                            <br><br>
                            @if (count($anggotakebersihan) > 0)
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Nama</th>
                                                <th scope="col">KTP</th>
                                                <th scope="col">Tim Kebersihan</th>
                                                <th scope="col">Tempat Lahir</th>
                                                <th scope="col">Tanggal Lahir</th>
                                                <th scope="col">Jenis Kelamin</th>
                                                <th scope="col">Alamat</th>
                                                <th scope="col">Nomor HP</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($anggotakebersihan as $ke)
                                                <tr>
                                                    <td class="text-center">{{ $loop->iteration }}</td>
                                                    <td>{{ $ke->nama }}</td>
                                                    <td>{{ $ke->ktp }}</td>
                                                    <td>{{ $ke->tim->nama_tim}}</td>
                                                    <td>{{ $ke->tempat_lahir }}</td>
                                                    <td>{{ $ke->tanggal_lahir }}</td>
                                                    <td>{{ $ke->jenis_kelamin }}</td>
                                                    <td>{{ $ke->alamat }}</td>
                                                    <td>{{ $ke->nomor_hp }}</td>
                                                    <td class="text-right">
                                                        <div class="dropdown">
                                                            <a class="btn btn-sm btn-icon-only text-light" href="#"
                                                                role="button" data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                                <i class="fas fa-ellipsis-v"></i>
                                                            </a>
                                                            <div
                                                                class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                                <a class="dropdown-item"
                                                                    href="/anggota_kebersihan/edit/{{ $ke->id }}">Edit</a>
                                                                <a class="dropdown-item"
                                                                    href="/anggota_kebersihan/destroy/{{ $ke->id }}">Hapus</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <br />
                                    {{-- Halaman : {{ $kebersihan->currentPage() }} <br/>
                    Jumlah Data : {{ $kebersihan->total() }} <br/>
                    Data Per Halaman : {{ $kebersihan->perPage() }} <br/>


                    {{ $kebersihan->links() }} --}}

                                </div>
                            @else
                                <br>
                                <div class="col-12 text-center">
                                    <span class="badge badge-warning">Belum ada data</span>
                                </div>
                            @endif
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">

                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                @include('layouts.footers.auth')
            </div>
        @endsection

        @push('js')
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
            <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        @endpush
