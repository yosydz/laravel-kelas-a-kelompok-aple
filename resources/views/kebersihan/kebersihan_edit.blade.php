@extends('layouts.app')

@section('content')
@include('layouts.headers.cards')

{{-- <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style> --}}

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="xyz align-items-center">
                        <div class="col-12">
                            <h3 class="mb-0">Form Edit Anggota Kebersihan</h3>
                        </div>
                        <p>
                        <form method="POST" action="/anggota_kebersihan/update/{{ $anggotakebersihan->id }}">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="nama" placeholder="Nama lengkap" value="{{ $anggotakebersihan->nama }}">
                                        @if ($errors->has('nama'))
                                        <div class="text-danger">
                                            {{ $errors->first('nama') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="custom-select" name="m_id_tim_kebersihan">
                                            <option value="{{ $anggotakebersihan->m_id_tim_kebersihan }}" selected>{{ $anggotakebersihan->tim->nama_tim }}</option>
                                            @foreach ($tim as $t)
                                            <option value="{{ $t->id }}">{{ $t->nama_tim }}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="ktp" placeholder="KTP" value="{{ $anggotakebersihan->ktp }}">
                                        @if ($errors->has('kebersihan'))
                                        <div class="text-danger">
                                            {{ $errors->first('kebersihan') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat lahir" value="{{ $anggotakebersihan->tempat_lahir }}">
                                        @if ($errors->has('tempat_lahir'))
                                        <div class="text-danger">
                                            {{ $errors->first('tempat_lahir') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Tanggal Lahir" id="date" name="tanggal_lahir" type="text" value="{{ $anggotakebersihan->tanggal_lahir }}">
                                            @if ($errors->has('tanggal_lahir'))
                                            <div class="text-danger">
                                                {{ $errors->first('tanggal_lahir') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    Jenis Kelamin : &nbsp;&nbsp;&nbsp;
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" value="Pria" {{ $anggotakebersihan->jenis_kelamin == 'Pria' ? 'checked' : '' }}>
                                            Pria
                                        </label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" value="Wanita" {{ $anggotakebersihan->jenis_kelamin == 'Wanita' ? 'checked' : '' }}>
                                            Wanita
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="alamat" placeholder="Alamat" value="{{ $anggotakebersihan->alamat }}">
                                        @if ($errors->has('alamat'))
                                        <div class="text-danger">
                                            {{ $errors->first('alamat') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="nomor_hp" placeholder="Nomor HP" value="{{ $anggotakebersihan->nomor_hp }}">
                                        @if ($errors->has('nomor_hp'))
                                        <div class="text-danger">
                                            {{ $errors->first('nomor_hp') }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-success">Edit</button>
                                </div>
                            </div>
                        </form>
                        <div class="card-footer py-4">
                            <nav class="d-flex justify-content-end" aria-label="...">

                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            @include('layouts.footers.auth')
        </div>
        @endsection

        @push('js')
        <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
        <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

        <script type="text/javascript">
            $(function() {
                $("#date").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    todayHighlight: true,
                    language: 'id'
                });
                $("#date2").datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true,
                    todayHighlight: true,
                    language: 'id'
                });
            });
        </script>
        @endpush
