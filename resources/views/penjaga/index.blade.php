@extends('layouts.app')

@section('content')
@include('layouts.headers.cards')

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Penjaga</h3>
                        </div>
                        <div class="col-4 text-right">
                            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#CreatePenjagaModal">Tambah Penjaga</button>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                <div class="table-responsive py-4">
                    <table class="table table-flush datatable">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>KTP</th>
                                <th>No. Telp</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">

                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- Detail Penjaga Modal -->
    <div class="modal" id="DetailPenjagaModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Detail Data Penjaga</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success! </strong>Penjaga was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input readonly type="text" class="form-control" name="detail-nama" id="detail-nama_penjaga">
                    </div>
                    <div class="form-group">
                        <label for="nik">NIK</label>
                        <input readonly type="text" class="form-control" name="detail-nik" id="detail-ktp">
                    </div>
                    <div class="form-group">
                        <label for="tempat">Tempat Lahir</label>
                        <input readonly type="text" class="form-control" name="detail-tempat" id="detail-tempat_lahir">
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Tanggal Lahir</label>
                        <input readonly type="date" class="form-control" name="detail-tanggal" id="detail-tanggal_lahir">
                    </div>
                    <div class="form-group">
                        <label for="jk">Jenis Kelamin</label>
                        <input readonly type="text" class="form-control" name="detail-jk" id="detail-jenis_kelamin">
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input readonly type="text" class="form-control" name="detail-alamat" id="detail-alamat">
                    </div>
                    <div class="form-group">
                        <label for="nohp">Nomor Hp</label>
                        <input readonly type="text" class="form-control" name="detail-nohp" id="detail-nomor_hp">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Create Penjaga Modal -->
    <div class="modal" id="CreatePenjagaModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Penjaga</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success! </strong>Penjaga was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama">
                    </div>
                    <div class="form-group">
                        <label for="nik">NIK</label>
                        <input type="text" class="form-control" name="nik" id="nik">
                    </div>
                    <div class="form-group">
                        <label for="tempat">Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat" id="tempat">
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="tanggal" id="tanggal">
                    </div>
                    <div class="form-group">
                        <label for="jk">Jenis Kelamin</label>
                        <select class="text-center form-control" style="width: 100%" name="jk" id="jk">
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="Pria">Pria</option>
                            <option value="Wanita">Wanita</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" name="alamat" id="alamat">
                    </div>
                    <div class="form-group">
                        <label for="nohp">Nomor Hp</label>
                        <input type="text" class="form-control" name="nohp" id="nohp">
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="SubmitCreatePenjagaForm">Create</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit Penjaga Modal -->
    <div class="modal" id="EditPenjagaModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Penjaga Edit</h4>
                    <button type="button" class="close modelClose" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="display: none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
                        <strong>Success! </strong>Penjaga was added successfully.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="form-group">
                        <label for="title">Nama</label>
                        <input type="text" class="form-control" name="edit-nama" id="edit-nama_penjaga">
                        <input type="hidden" class="form-control" name="edit-id" id="edit-id">
                    </div>
                    <div class="form-group">
                        <label for="nik">NIK</label>
                        <input type="text" class="form-control" name="edit-nik" id="edit-ktp">
                    </div>
                    <div class="form-group">
                        <label for="tempat">Tempat Lahir</label>
                        <input type="text" class="form-control" name="edit-tempat" id="edit-tempat_lahir">
                    </div>
                    <div class="form-group">
                        <label for="tanggal">Tanggal Lahir</label>
                        <input type="date" class="form-control" name="edit-tanggal" id="edit-tanggal_lahir">
                    </div>
                    <div class="form-group">
                        <label for="jk">Jenis Kelamin</label>
                        <select class="text-center form-control" style="width: 100%" id="edit-jenis_kelamin" name="jk">
                            <option value="">Pilih Jenis Kelamin</option>
                            <option value="Pria">Pria</option>
                            <option value="Wanita">Wanita</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" name="edit-alamat" id="edit-alamat">
                    </div>
                    <div class="form-group">
                        <label for="nohp">Nomor Hp</label>
                        <input type="text" class="form-control" name="edit-nohp" id="edit-nomor_hp">
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="SubmitEditPenjagaForm">Update</button>
                    <button type="button" class="btn btn-danger modelClose" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Delete Penjaga Modal -->
    <div class="modal" id="DeletePenjagaModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Penjaga Delete</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <h4>Are you sure want to delete this Penjaga?</h4>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="SubmitDeletePenjagaForm">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footers.auth')
</div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
     <!-- Optional JS -->
    <script src="{{ asset('argon') }}/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/datatables.net-select/js/dataTables.select.min.js"></script>

    <script src="https://cdn.datatables.net/fixedcolumns/3.3.2/js/dataTables.fixedColumns.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.8/js/dataTables.fixedHeader.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <script type="text/javascript">

        // Update Penjaga Ajax request.

        function show(id){
            var url = '{{ route("penjaga.show", ":id") }}';
            url = url.replace(':id', id);
            $.getJSON( url, function(data){
                if(data.error == 0) {
                    console.log(data.nama_penjaga);
                    $('#detail-nama_penjaga').val(data.nama_penjaga);
                    $('#detail-ktp').val(data.ktp);
                    $('#detail-tempat_lahir').val(data.tempat_lahir);
                    $('#detail-tanggal_lahir').val(data.tanggal_lahir);
                    $('#detail-jenis_kelamin').val(data.jenis_kelamin);
                    $('#detail-alamat').val(data.alamat);
                    $('#detail-nomor_hp').val(data.nomor_hp);
                    $("#DetailPenjagaModal").modal("show");
                } else {
                    $('.alert-danger').html('');
                    $('.alert-danger').show();
                    $('.alert-danger').append('<strong><li>Error</li></strong>');
                }
            });
        }

        function edit(id){
            var url = '{{ route("penjaga.show", ":id") }}';
            url = url.replace(':id', id);
            $.getJSON( url, function(data){
                if(data.error == 0) {
                    $('#edit-id').val(data.id_penjaga);
                    $('#edit-nama_penjaga').val(data.nama_penjaga);
                    $('#edit-ktp').val(data.ktp);
                    $('#edit-tempat_lahir').val(data.tempat_lahir);
                    $('#edit-tanggal_lahir').val(data.tanggal_lahir);
                    $('#edit-jenis_kelamin').val(data.jenis_kelamin);
                    $('#edit-alamat').val(data.alamat);
                    $('#edit-nomor_hp').val(data.nomor_hp);
                    $("#EditPenjagaModal").modal("show");
                } else {
                    $('.alert-danger').html('');
                    $('.alert-danger').show();
                    $('.alert-danger').append('<strong><li>Error</li></strong>');
                }
            });

        }

        $(document).ready(function() {
        // Delete Penjaga Ajax request.
            var deleteID;
            $('body').on('click', '#getDeleteId', function(){
                deleteID = $(this).data('id');
            })
            $('#SubmitDeletePenjagaForm').click(function(e) {
                e.preventDefault();
                var id = deleteID;
                var url = '{{ route("penjaga.destroy", ":id") }}';
                url = url.replace(':id', id);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    method: 'DELETE',
                    success: function(result) {
                        $('.alert-danger').hide();
                        $('.alert-success').show();
                        $('.datatable').DataTable().ajax.reload();
                        setInterval(function(){
                            $('.alert-success').hide();
                            $("#DeletePenjagaModal").modal("hide");
                            location.reload();
                        }, 1500);
                    }
                });
            });

            //edit
            $('#SubmitEditPenjagaForm').click(function(e) {
                var id = $('#edit-id').val();
                // console.log(id);
                var url = '{{ route("penjaga.update", ":id") }}';
                url = url.replace(':id', id);
                $('#EditPenjagaModal').animate({ scrollTop: 0 }, 'slow');
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: url,
                    method: 'PUT',
                    data: {
                        nama_penjaga: $('#edit-nama_penjaga').val(),
                        ktp:$('#edit-ktp').val(),
                        tempat_lahir:$('#edit-tempat_lahir').val(),
                        tanggal_lahir:$('#edit-tanggal_lahir').val(),
                        jenis_kelamin:$('#edit-jenis_kelamin').val(),
                        alamat:$('#edit-alamat').val(),
                        nomor_hp:$('#edit-nomor_hp').val(),
                    },
                    success: function(result) {
                        if(result.errors) {
                            $('.alert-danger').html('');
                            $.each(result.errors, function(key, value) {
                                $('.alert-danger').show();
                                $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                            });
                        } else {
                            $('.alert-danger').hide();
                            $('.alert-success').show();
                            $('.datatable').DataTable().ajax.reload();
                            setInterval(function(){
                                $('.alert-success').hide();
                                $("#EditPenjagaModal").modal("hide");
                                location.reload();
                            }, 1500);
                        }
                    }
                });
            });

            // init datatable.
            var dataTable = $('.datatable').DataTable({
                "columnDefs": [ {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                } ],

                processing: true,
                serverSide: true,
                autoWidth: false,
                // scrollX: true,
                ajax: '{{ route('data') }}',
                columns: [
                    {"defaultContent": ""},
                    {data: 'nama_penjaga', name: 'nama_penjaga'},
                    {data: 'ktp', name: 'ktp'},
                    {data: 'nomor_hp', name: 'nomor_hp'},
                    {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
                ]
            });

            dataTable.on( 'draw.dt', function () {
            var PageInfo = $('.datatable').DataTable().page.info();
            dataTable.column(0, { page: 'current' }).nodes().each( function (cell, i) {
                    cell.innerHTML = i + 1 + PageInfo.start;
                } );
            });

            // Create Ajax request.
            $('#SubmitCreatePenjagaForm').click(function(e) {
                // console.log("click cok");
                $('#CreatePenjagaModal').animate({ scrollTop: 0 }, 'slow');
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{ route('penjaga.store') }}",
                    method: 'post',
                    data: {
                        nama_penjaga: $('#nama').val(),
                        ktp: $('#nik').val(),
                        tempat_lahir: $('#tempat').val(),
                        tanggal_lahir: $('#tanggal').val(),
                        jenis_kelamin: $('#jk').val(),
                        alamat: $('#alamat').val(),
                        nomor_hp: $('#nohp').val(),
                    },
                    success: function(result) {
                        if(result.errors) {
                            $('.alert-danger').html('');
                            $.each(result.errors, function(key, value) {
                                $('.alert-danger').show();
                                $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                            });
                        } else {
                            $('.alert-danger').hide();
                            $('.alert-success').show();
                            $('.datatable').DataTable().ajax.reload();
                            setInterval(function(){
                                $('.alert-success').hide();
                                $('#CreatePenjagaModal').modal('hide');
                                location.reload();
                            }, 2000);
                        }
                    }
                });
            });
        });
    </script>
@endpush
