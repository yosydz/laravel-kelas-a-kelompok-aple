@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    {{-- <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style> --}}

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Form Edit Kegiatan</h3>
                            </div>
                            <hr>
                            <form method="post" action="/kegiatan/update/{{ $kegiatan->id_kegiatan }}">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="nama_kegiatan"
                                                placeholder="Nama Kegiatan" value="{{ $kegiatan->nama_kegiatan }}">
                                            @if ($errors->has('nama_kegiatan'))
                                                <div class="text-danger">
                                                    {{ $errors->first('nama_kegiatan') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="instansi" placeholder="Instansi"
                                                value="{{ $kegiatan->instansi }}">
                                            @if ($errors->has('instansi'))
                                                <div class="text-danger">
                                                    {{ $errors->first('instansi') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="custom-select" name="m_id_peminjam">
                                                <option value="{{ $kegiatan->m_id_peminjam }}" selected>{{ $kegiatan->peminjam->nama_peminjam }}</option>
                                                @foreach ($peminjam as $pem)
                                                    <option value="{{ $pem->id_peminjam }}">{{ $pem->nama_peminjam }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="ni ni-calendar-grid-58"></i></span>
                                                </div>
                                                <input class="form-control" placeholder="Tanggal Mulai" id="date"
                                                    name="tanggal_mulai" type="text"
                                                    value="{{ $kegiatan->tanggal_mulai }}">
                                                @if ($errors->has('tanggal_mulai'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('tanggal_mulai') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i
                                                            class="ni ni-calendar-grid-58"></i></span>
                                                </div>
                                                <input class="form-control" placeholder="Tanggal Akhir" id="date2"
                                                    name="tanggal_akhir" type="text"
                                                    value="{{ $kegiatan->tanggal_akhir }}">
                                                @if ($errors->has('tanggal_akhir'))
                                                    <div class="text-danger">
                                                        {{ $errors->first('tanggal_akhir') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-success">Simpan</button>
                                    </div>
                                </div>
                            </form>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">

                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                @include('layouts.footers.auth')
            </div>
        @endsection

        @push('js')
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
            <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

            <script type="text/javascript">
                $(function() {
                    $("#date").datepicker({
                        format: 'yyyy-mm-dd',
                        autoclose: true,
                        todayHighlight: true,
                        language: 'id'
                    });
                    $("#date2").datepicker({
                        format: 'yyyy-mm-dd',
                        autoclose: true,
                        todayHighlight: true,
                        language: 'id'
                    });
                });

            </script>
        @endpush
