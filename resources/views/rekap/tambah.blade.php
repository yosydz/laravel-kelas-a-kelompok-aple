@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    
    <div class="container-fluid mt--7">
        
        <div class="row mt-5">
            <div class="col-xl-8 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Tambah Rekapitulasi</h3>
                            </div>
                        </div>
                    </div>
                    <form method="post" action="/rekap">
                        <div class="col-md-12">

                            @csrf
                            <div class="form-group">
                                <label for="pilihGedung">Pilih Gedung</label>
                                <select class="form-control" id="m_id_gedung" name="m_id_gedung">
                                        <option value="pilih">Nama Gedung ...</option>
                                    @foreach ($gedung as $gd)
                                        <option value="{{ $gd->id_gedung }}">{{ $gd->nama_gedung }}</option>
                                        
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pilihKegiatan">Pilih Kegiatan</label>
                                <select class="form-control" id="m_id_kegiatan" name="m_id_kegiatan">
                                        <option value="pilih">Nama Kegiatan ...</option>
                                    @foreach ($kegiatan as $kg)
                                        <option value="{{ $kg->id_kegiatan }}">{{ $kg->nama_kegiatan }}</option>
                                            
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group text-right">
                            <button type="submit" class="btn btn-primary">Tambah Rekap</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush