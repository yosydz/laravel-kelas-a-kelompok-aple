@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">

        <div class="row mt-5">
            <div class="col-xl-12 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Rekapitulasi</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ url('/rekap/tambah') }}" class="btn btn-md btn-primary">Tambah Rekap</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">No.</th>
                                    <th scope="col">Peminjam</th>
                                    <th scope="col">Gedung</th>
                                    <th scope="col">Nama Kegiatan</th>
                                    <th scope="col">Mulai Kegiatan</th>
                                    <th scope="col">Akhir Kegiatan</th>
                                    <th scope="col">Penjaga</th>
                                    <th scope="col">Kebersihan</th>
                                    <th scope="col">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php $no = 1;?>
                            @foreach ($rekap as $rkp)
                                <tr>
                                    <th scope="row">
                                        {{ $no }}
                                    </th>
                                    <td>
                                        {{ $rkp->nama_peminjam }}
                                    </td>
                                    <td>
                                        {{ $rkp->nama_gedung }}
                                    </td>
                                    <td>
                                        {{ $rkp->nama_kegiatan }}
                                    </td>
                                    <td>
                                        {{ $rkp->tanggal_mulai }}
                                    </td>
                                    <td>
                                        {{ $rkp->tanggal_akhir }}
                                    </td>
                                    <td>
                                        {{ $rkp->nama_penjaga }}
                                    </td>
                                    <td>
                                        {{ $rkp->nama_tim }}
                                    </td>
                                    <td>
                                        <a href="/rekap/edit/{{ $rkp->id_rekap }}"><span class="badge badge-md badge-primary">Edit</span></a>
                                        <a href="/rekap/delete/{{ $rkp->id_rekap }}"><span class="badge badge-md badge-warning">Hapus</span></a>
                                    </td>
                                </tr>
                                <?php $no++ ; ?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush
