@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    {{-- <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style> --}}

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Tim</h3>
                                @if ($message = Session::get('success'))
                                    <div class="alert alert-success">
                                        <p>{{ $message }}</p>
                                    </div>
                                @endif
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('tim.create') }}" class="btn btn-sm btn-primary">Tambah Tim</a>
                            </div>
                            <br>
                            <br>
                            @if (count($datas) > 0)
                                <div class="table-responsive">
                                    <table class="table table-condensed table-bordered table-hover table-flush">
                                        <thead class="thead-light">
                                            <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Nama Tim</th>
                                                <th class="text-center">Opsi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($datas as $data)
                                                <tr>
                                                    <td class="text-center">{{ $loop->iteration }}</td>
                                                    <td class="text-center">{{ $data->nama_tim }}</td>
                                                    <td class="text-center">
                                                        <a type="button" href="{{ route('tim.edit', $data->id) }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a>
                                                        <form class="d-inline" method="POST" action="{{ route('tim.destroy', $data->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <br />
                                </div>
                            @else
                                <br>
                                <div class="col-12 text-center">
                                    <span class="badge badge-warning">Belum ada data</span>
                                </div>
                            @endif
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">

                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                @include('layouts.footers.auth')
            </div>
        @endsection

        @push('js')
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
            <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        @endpush
