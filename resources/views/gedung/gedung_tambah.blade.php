@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="xyz align-items-center">
                            <div class="col-12">
                                <h3 class="mb-0">Form Tambah Gedung</h3>
                            </div>
                            <p>
                            <form method="POST" action="/gedung/store">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="nama_gedung"
                                                placeholder="Nama Gedung">
                                            @if ($errors->has('nama_gedung'))
                                                <div class="text-danger">
                                                    {{ $errors->first('nama_gedung') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="kapasitas" placeholder="Kapasitas">
                                            @if ($errors->has('kapasitas'))
                                                <div class="text-danger">
                                                    {{ $errors->first('kapasitas') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="fasilitas" placeholder="Fasilitas">
                                            @if ($errors->has('fasilitas'))
                                                <div class="text-danger">
                                                    {{ $errors->first('fasilitas') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="luas" placeholder="Luas">
                                            @if ($errors->has('luas'))
                                                <div class="text-danger">
                                                    {{ $errors->first('luas') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="text-center form-control" style="width: 100%" name="m_id_penjaga">
                                                <option value="">Pilih Penjaga</option>
                                                @foreach($penjaga as $pjg)
                                                    <option value="{{$pjg->id_penjaga}}">{{$pjg->nama_penjaga}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="text-center form-control" style="width: 100%" name="m_id_tim_kebersihan">
                                                <option value="">Pilih Tim Kebersihan</option>
                                                @foreach($tim as $t)
                                                    <option value="{{$t->id}}">{{$t->nama_tim}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <button type="submit" class="btn btn-success">Tambahkan</button>
                                    </div>
                                </div>
                            </form>
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">

                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                @include('layouts.footers.auth')
            </div>
        @endsection

        @push('js')
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
            <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        @endpush
