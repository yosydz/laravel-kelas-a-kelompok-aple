@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    {{-- <style type="text/css">
		.pagination li{
			float: left;
			list-style-type: none;
			margin:5px;
		}
	</style> --}}

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Gedung</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="/gedung/tambah" class="btn btn-sm btn-primary">Tambah Gedung</a>
                            </div>
                            <br><br><br>
                            @if (count($gedung) > 0)
                                <div class="table-responsive">
                                    <table class="table align-items-center table-flush">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">No</th>
                                                <th scope="col">Nama Gedung</th>
                                                <th scope="col">Kapasitas</th>
                                                <th scope="col">Fasilitas</th>
                                                <th scope="col">Tim Kebersihan</th>
                                                <th scope="col">Luas</th>
                                                <th scope="col">Nama Penjaga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($gedung as $g)
                                            {{-- {{ dd($g->penjaga->nama_penjaga) }} --}}
                                                <tr>
                                                    <td class="text-center">{{ $loop->iteration }}</td>
                                                    <td>{{ $g->nama_gedung }}</td>
                                                    <td>{{ $g->kapasitas }}</td>
                                                    <td>{{ $g->fasilitas }}</td>
                                                    <td>{{ $g->tim->nama_tim }}</td>
                                                    <td>{{ $g->luas }}</td>
                                                    <td>{{ $g->penjaga->nama_penjaga}}</td>
                                                    <td class="text-right">
                                                        <div class="dropdown">
                                                            <a class="btn btn-sm btn-icon-only text-light" href="#"
                                                                role="button" data-toggle="dropdown" aria-haspopup="true"
                                                                aria-expanded="false">
                                                                <i class="fas fa-ellipsis-v"></i>
                                                            </a>
                                                            <div
                                                                class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                                <a class="dropdown-item"
                                                                    href="/gedung/edit/{{ $g->id_gedung }}">Edit</a>
                                                                <a class="dropdown-item"
                                                                    href="/gedung/hapus/{{ $g->id_gedung }}">Hapus</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <br />
                                    {{-- Halaman : {{ $gedung->currentPage() }} <br/>
                    Jumlah Data : {{ $gedung->total() }} <br/>
                    Data Per Halaman : {{ $gedung->perPage() }} <br/>


                    {{ $gedung->links() }} --}}

                                </div>
                            @else
                                <br>
                                <div class="col-12 text-center">
                                    <span class="badge badge-warning">Belum ada data</span>
                                </div>
                            @endif
                            <div class="card-footer py-4">
                                <nav class="d-flex justify-content-end" aria-label="...">

                                </nav>
                            </div>
                        </div>
                    </div>
                </div>

                @include('layouts.footers.auth')
            </div>
        @endsection

        @push('js')
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
            <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
            <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        @endpush
