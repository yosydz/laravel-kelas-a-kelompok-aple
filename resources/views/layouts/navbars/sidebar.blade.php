<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
            aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Brand -->
        <a class="navbar-brand pt-0" href="{{ route('home') }}">
            <img src="{{ asset('images') }}/ub_logo_l.png" class="navbar-brand-img" alt="...">
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/team-1-800x800.jpg">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-settings-gear-65"></i>
                        <span>{{ __('Settings') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-calendar-grid-58"></i>
                        <span>{{ __('Activity') }}</span>
                    </a>
                    <a href="#" class="dropdown-item">
                        <i class="ni ni-support-16"></i>
                        <span>{{ __('Support') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            <img src="{{ asset('argon') }}/img/brand/blue.png">
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse"
                            data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                            aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge">
                    <input type="search" class="form-control form-control-rounded form-control-prepended"
                        placeholder="{{ __('Search') }}" aria-label="Search">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="fa fa-search"></span>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('home') ? 'active' : '' }}" href="{{ route('home') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Dashboard') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('rekap') || request()->is('rekap/tambah') || request()->is('rekap/edit/*') ? 'active' : '' }}" href="{{ route('rekap') }}">
                        <i class="ni ni-collection text-blue"></i> {{ __('Rekapitulasi') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('kegiatan') || request()->is('kegiatan/tambah') || request()->is('kegiatan/edit/*') ? 'active' : '' }}"
                        href="{{ route('kegiatan') }}">
                        <i class="ni ni-calendar-grid-58 text-blue"></i> {{ __('Kegiatan') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('gedung') || request()->is('gedung/tambah') || request()->is('gedung/edit/*') ? 'active' : '' }}"
                        href="{{ route('gedung') }}">
                        <i class="fa fa-building text-blue"></i> {{ __('Gedung') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('peminjam') || request()->is('tim') || request()->is('penjaga') ? 'active' : '' }}" href="#navbar-examples" data-toggle="collapse" role="button"
                        aria-expanded="true" aria-controls="navbar-examples">
                        <i class="fa fa-atom text-red"></i>
                        <span class="nav-link-text" style="color: #f4645f;">{{ __('Data Master') }}</span>
                    </a>
                    <div class="collapse {{ request()->is('peminjam') || request()->is('peminjam/create') || request()->is('peminjam/edit/*')
                    || request()->is('tim') || request()->is('tim/create') || request()->is('tim/*/edit')
                    || request()->is('penjaga') ? 'show' : '' }}" id="navbar-examples">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('peminjam') || request()->is('peminjam/create') || request()->is('peminjam/edit/*') ? 'active' : '' }}"
                                    href="{{ route('peminjam') }}">
                                    <i class="ni ni-calendar-grid-58 text-blue"></i> {{ __('Data Peminjam') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('tim') || request()->is('tim/create') || request()->is('tim/*/edit') ? 'active' : '' }}"
                                    href="{{ route('tim.index') }}">
                                    <i class="fas fa-users text-blue"></i> {{ __('Tim Kebersihan') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ request()->is('penjaga') ? 'active' : '' }}"
                                    href="{{ route('penjaga.index') }}">
                                    <i class="fas fa-user-secret text-blue"></i> {{ __('Penjaga') }}</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('anggota_kebersihan') || request()->is('anggota_kebersihan/create') || request()->is('anggota_kebersihan/edit/*') ? 'active' : '' }}"
                        href="{{ route('anggota_kebersihan') }}">
                        <i class="fas fa-user text-blue"></i> {{ __('Anggota Kebersihan') }}
                    </a>
                </li>
            </ul>
            </ul>
        </div>
    </div>
</nav>
