@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Edit Data Peminjam</h3>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="/peminjam/update/{{ $peminjam->id_peminjam }}"
                        class="container-fluid ml--19">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" name="nama_peminjam" class="form-control" placeholder="Nama Peminjam"
                                        value="{{ $peminjam->nama_peminjam }}">
                                </div>
                                @if ($errors->has('nama_peminjam'))
                                    <div class="text-danger">
                                        {{ $errors->first('nama_peminjam') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Status" type="text" name="status"
                                        value="{{ $peminjam->status }}">
                                </div>
                                @if ($errors->has('status'))
                                    <div class="text-danger">
                                        {{ $errors->first('status') }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Nomor KTP / NIM" type="text" name="nim_ktp"
                                        value="{{ $peminjam->nim_ktp }}">
                                    @if ($errors->has('nim_ktp'))
                                        <div class="text-danger">
                                            {{ $errors->first('nim_ktp') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                Jenis Kelamin : &nbsp;&nbsp;&nbsp;
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin" value="Laki-laki"
                                            {{ $peminjam->jenis_kelamin == 'Laki-laki' ? 'checked' : '' }}>
                                        Laki-laki
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="jenis_kelamin" value="Perempuan"
                                            {{ $peminjam->jenis_kelamin == 'Perempuan' ? 'checked' : '' }}>
                                        Perempuan
                                    </label>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" placeholder="nomor HP / WA" type="text" name="nomor_hp"
                                        value="{{ $peminjam->nomor_hp }}">
                                </div>
                                @if ($errors->has('nomor_hp'))
                                    <div class="text-danger">
                                        {{ $errors->first('nomor_hp') }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" type="email" name="email"
                                        value="{{ $peminjam->email }}">
                                    @if ($errors->has('email'))
                                        <div class="text-danger">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-success">Simpan</button>
                            </div>
                        </div>
                    </form>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
    <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

    <script type="text/javascript">
        $(function() {
            $("#date").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
                language: 'id'
            });
            $("#date2").datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                todayHighlight: true,
                language: 'id'
            });
        });

    </script>
@endpush
