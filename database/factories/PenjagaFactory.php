<?php

namespace Database\Factories;

use App\Models\Penjaga;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as Faker;

class PenjagaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Penjaga::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = Faker::create('id_ID');
        return [
            "nama_penjaga" => $faker->name,
            "ktp" => $faker->numerify('##########'),
            "tempat_lahir" => $faker->city,
            "tanggal_lahir" => $faker->dateTimeThisCentury->format('Y-m-d'),
            "jenis_kelamin" => $faker->randomElement(['Pria', 'Wanita']),
            "alamat" => $faker->address,
            "nomor_hp" => $faker->phoneNumber,
        ];
    }
}
