<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use DB;

class GedungSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data faker indonesia
        $faker = Faker::create('id_ID');

        // membuat data dummy sebanyak 10 record
        for($x = 1; $x <= 5; $x++){

        	// insert data dummy pegawai dengan faker
        	DB::table('gedung')->insert([
        		'nama_gedung' => $faker->randomElement(['Samantha Krida', 'GOR Pertamina', 'GKM', 'Widyaloka', 'Gazebo UB']),
        		'kapasitas' => $faker->buildingNumber,
                'fasilitas' => $faker->macProcessor,
                'luas' => $faker->randomElement(['100', '200', '3091']),
                'm_id_penjaga' => $faker->randomElement(['1', '2', '3']),
                'm_id_tim_kebersihan' => $faker->randomElement(['1', '2', '3']),

        	]);
        }
    }
}
