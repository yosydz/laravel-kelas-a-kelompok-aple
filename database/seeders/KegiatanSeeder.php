<?php

namespace Database\Seeders;

use DB;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class KegiatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data faker indonesia
        $faker = Faker::create('id_ID');

        // membuat data dummy sebanyak 10 record
        for ($x = 1; $x <= 5; $x++) {

            // insert data dummy pegawai dengan faker
            DB::table('kegiatan')->insert([
                'nama_kegiatan' => $faker->name,
                'instansi' => $faker->company,
                'tanggal_mulai' => $faker->date,
                'tanggal_akhir' => $faker->date,
                'm_id_peminjam' => $faker->randomElement(['1', '2', '3']),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        }
    }
}
