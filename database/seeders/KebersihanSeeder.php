<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use DB;

class KebersihanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data faker indonesia
        $faker = Faker::create('id_ID');

        // membuat data dummy sebanyak 10 record
        for($x = 1; $x <= 10; $x++){

        	// insert data dummy pegawai dengan faker
        	DB::table('anggota_kebersihan')->insert([
        		'nama' => $faker->name,
        		'ktp' => $faker->company,
                'tempat_lahir' => $faker->date,
                'tanggal_lahir' => $faker->date,
                'jenis_kelamin' => $faker->date,
                'alamat' => $faker-> name,
                'nomor_hp' => $faker-> name,

        	]); 
        }
    }
}
