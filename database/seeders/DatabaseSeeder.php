<?php
namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call([UsersTableSeeder::class]);
        $this->call([PenjagaSeeder::class]);
        $this->call([UsersTableSeeder::class]);
        $this->call([KegiatanSeeder::class]);
        $this->call([PeminjamTableSeeder::class]);

    }
}
