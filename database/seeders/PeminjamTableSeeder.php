<?php

namespace Database\Seeders;

use DB;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class PeminjamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // data faker indonesia
$faker = Faker::create('id_ID');

// membuat data dummy sebanyak 10 record
for ($x = 1; $x <= 5; $x++) {

    // insert data dummy pegawai dengan faker
    DB::table('peminjam')->insert([
        'nama_peminjam' => $faker->name,
        'status' => $faker->randomElement(['Mahasiswa', 'Umum']),
        'nim_ktp' => $faker->e164PhoneNumber,
        'jenis_kelamin' => $faker->randomElement(['Laki-laki', 'Perempuan']),
        'nomor_hp' => $faker->e164PhoneNumber,
        'email' => $faker->freeEmail,
        // 'm_id_kegiatan' => $faker->randomElement(['1', '2']),
        'created_at' => now(),
        'updated_at' => now()

    ]);

}

    }
}
