<?php

namespace Database\Seeders;

use App\Models\Penjaga;
use Illuminate\Database\Seeder;

class PenjagaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Penjaga::factory()->count(10)->create();
    }
}
